package com.example.chaitanya.kisantestapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.chaitanya.kisantestapp.R;

public class ContactInfo extends AppCompatActivity implements View.OnClickListener {
    TextView fullname;
    TextView mobileno;
    Button sendmessage;
    String firstname;
    String lastname;
    String mobilenumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactinfo);
        firstname = getIntent().getStringExtra("firstname");
        lastname = getIntent().getStringExtra("lastname");
        mobilenumber = getIntent().getStringExtra("mobileno");
        fullname = (TextView)findViewById(R.id.fullname);
        mobileno =(TextView)findViewById(R.id.mobilenobtn);
        sendmessage = (Button)findViewById(R.id.sendbtn);
        sendmessage.setOnClickListener(this);
        fullname.setText(firstname+" "+lastname);
        mobileno.setText(mobilenumber);
    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent(this,ComposeMessage.class);
        i.putExtra("mobileno",mobilenumber);
        i.putExtra("firstname",firstname);
        i.putExtra("lastname",lastname);
        startActivity(i);
    }
}
