package com.example.chaitanya.kisantestapp.fragments;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.chaitanya.kisantestapp.R;
import com.example.chaitanya.kisantestapp.activities.MainActivity;
import com.example.chaitanya.kisantestapp.adapters.RecipientListAdapter;
import com.example.chaitanya.kisantestapp.pojos.SentMessage;
import com.example.chaitanya.kisantestapp.pojos.UserContact;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class RecipientListFragment extends Fragment {
    View view;
    private LinearLayoutManager recyclerViewLayoutManager;
    private RecyclerView recyclerView;
    private RecipientListAdapter recyclerView_Adapter;
    Gson gson = new Gson();
    public RecipientListFragment(){
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recipientlistfragment,container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recipient_list);
        recyclerViewLayoutManager = new LinearLayoutManager(getActivity());
        String recipientlist = MainActivity.getDefaults("recipientlist",getActivity());
        Type sentMessagesListType = new TypeToken<List<SentMessage>>(){}.getType();
        List<SentMessage> sentMessages = gson.fromJson(recipientlist,sentMessagesListType);
        if(sentMessages != null) {
            Collections.reverse(sentMessages);
            recyclerView_Adapter = new RecipientListAdapter(getActivity(), sentMessages);
            recyclerView.setLayoutManager(recyclerViewLayoutManager);
            recyclerView.setAdapter(recyclerView_Adapter);
        }
        return view;
    }
}
