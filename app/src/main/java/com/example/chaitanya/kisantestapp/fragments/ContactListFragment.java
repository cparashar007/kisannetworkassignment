package com.example.chaitanya.kisantestapp.fragments;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.chaitanya.kisantestapp.R;
import com.example.chaitanya.kisantestapp.adapters.ContactListAdapter;
import com.example.chaitanya.kisantestapp.pojos.UserContact;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class ContactListFragment extends Fragment {
    View view;
    List<UserContact> contactList = new ArrayList<>();
    private LinearLayoutManager recyclerViewLayoutManager;
    private RecyclerView recyclerView;
    private ContactListAdapter recyclerView_Adapter;
    Gson gson = new Gson();

    public ContactListFragment(){
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contactlistfragment,container, false);
        recyclerView = view.findViewById(R.id.contact_list);
        recyclerViewLayoutManager = new LinearLayoutManager(getActivity());
        try {
            JSONArray obj = new JSONArray(loadJSONFromAsset());
            Type contactListType = new TypeToken<List<UserContact>>(){}.getType();
            contactList = gson.fromJson(obj.toString(), contactListType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        recyclerView_Adapter = new ContactListAdapter(getActivity(), contactList);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        recyclerView.setAdapter(recyclerView_Adapter);

        return view;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("contacts.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
