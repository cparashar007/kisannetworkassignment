package com.example.chaitanya.kisantestapp.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chaitanya.kisantestapp.R;
import com.example.chaitanya.kisantestapp.activities.ContactInfo;
import com.example.chaitanya.kisantestapp.pojos.UserContact;

import java.util.ArrayList;
import java.util.List;


public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {


    private Context context1;
    private List<UserContact> userContacts = new ArrayList<>();

    public ContactListAdapter(Context context2,List<UserContact> userContacts) {
        this.userContacts = userContacts;
        context1 = context2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView firstname;
        private TextView lastname;
        public ViewHolder(View v) {
            super(v);
            firstname = (TextView)v.findViewById(R.id.firstname);
            lastname = (TextView)v.findViewById(R.id.lastname);

        }
    }

    @Override
    public ContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(context1).inflate(R.layout.contactlistadapter, parent, false);

        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Vholder, final int position) {
        Vholder.firstname.setText(userContacts.get(position).getFirstName());
        Vholder.lastname.setText(userContacts.get(position).getLastName());
        Vholder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context1, ContactInfo.class);
                i.putExtra("firstname", userContacts.get(position).getFirstName());
                i.putExtra("lastname", userContacts.get(position).getLastName());
                i.putExtra("mobileno", userContacts.get(position).getMobileNo());
                context1.startActivity(i);
            }
        });

    }



    @Override
    public int getItemCount() {

        return userContacts.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
