package com.example.chaitanya.kisantestapp.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Looper;
import android.os.MessageQueue;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.chaitanya.kisantestapp.R;
import com.example.chaitanya.kisantestapp.pojos.SentMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ComposeMessage extends AppCompatActivity implements View.OnClickListener {
    EditText message;
    Button sendmessage;
    int otp;
    String mobilenumber;
    String firstname;
    String lastname;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.composemessage);
        mobilenumber = getIntent().getStringExtra("mobileno");
        firstname = getIntent().getStringExtra("firstname");
        lastname = getIntent().getStringExtra("lastname");
        isNetworkConnected();
        message = (EditText) findViewById(R.id.sendmessage);
        sendmessage = (Button)findViewById(R.id.sendcomposedmsg);
        sendmessage.setOnClickListener(this);
        Random ran = new Random();
        otp = (100000 + ran.nextInt(899999));
        message.setText("Hi,Your OTP is : "+otp);
    }



    @Override
    public void onClick(View view) {
        new Thread(new Runnable(){
            @Override
            public void run() {
                try {

                    HttpClient httpclient = new DefaultHttpClient();

                    HttpPost httppost = new HttpPost(
                            "https://api.twilio.com/2010-04-01/Accounts/AC8f7c93c6bea37fb2e8373b79ae8c5c16/SMS/Messages");
                    String base64EncodedCredentials = "Basic "
                            + Base64.encodeToString(
                            ("AC8f7c93c6bea37fb2e8373b79ae8c5c16" + ":" + "f4e14ca8bba16cddc48cbd573786e460").getBytes(),
                            Base64.NO_WRAP);

                    httppost.setHeader("Authorization",
                            base64EncodedCredentials);

                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("From",
                            "+17013803165"));
                    nameValuePairs.add(new BasicNameValuePair("To",
                            "+91"+mobilenumber));

                    nameValuePairs.add(new BasicNameValuePair("Body",
                            ""+message.getText()));

                    httppost.setEntity(new UrlEncodedFormEntity(
                            nameValuePairs));

                    // Execute HTTP Post Request
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    System.out.println("Entity post is: "
                            + EntityUtils.toString(entity));

                    Long time = System.currentTimeMillis();
                    String recipientlist = MainActivity.getDefaults("recipientlist",getApplicationContext());
                    Type sentMessagesListType = new TypeToken<List<SentMessage>>(){}.getType();
                    List<SentMessage> sentMessages = gson.fromJson(recipientlist,sentMessagesListType);
                    if(sentMessages == null){
                        sentMessages = new ArrayList<>();
                    }
                        SentMessage sentMessage = new SentMessage();
                        sentMessage.setFirstName(firstname);
                        sentMessage.setLastName(lastname);
                        sentMessage.setOtp(otp);
                        sentMessage.setTime(time);
                        sentMessages.add(sentMessage);
                        MainActivity.setDefaults("recipientlist", gson.toJson(sentMessages),getApplicationContext());

                    Intent i = new Intent(getApplicationContext(),MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
                catch (ClientProtocolException e) {
                    showToastInThread(getApplicationContext(),"Sms Sending Failed");
                }
                catch (IOException e) {
                    showToastInThread(getApplicationContext(),"Sms Sending Failed");
                }
                catch (Exception ex) {
                    showToastInThread(getApplicationContext(),"Sms Sending Failed");
                }
            }
        }).start();
    }




    private void isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo == null) {
            new AlertDialog.Builder(this)
                    .setMessage("No Internet Connection")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    })
                    .show();
        }
    }




    public void showToastInThread(final Context context,final String str){
        Looper.prepare();
        MessageQueue queue = Looper.myQueue();
        queue.addIdleHandler(new MessageQueue.IdleHandler() {
            int mReqCount = 0;

            @Override
            public boolean queueIdle() {
                if (++mReqCount == 0) {
                    Looper.myLooper().quit();
                    return false;
                } else {
                    return true;
                }
            }
        });
        Toast.makeText(context, str,Toast.LENGTH_SHORT).show();
        Looper.loop();
    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent(this,MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

}