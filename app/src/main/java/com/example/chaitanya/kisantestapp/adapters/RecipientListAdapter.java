package com.example.chaitanya.kisantestapp.adapters;



import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chaitanya.kisantestapp.R;
import com.example.chaitanya.kisantestapp.pojos.SentMessage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class RecipientListAdapter extends RecyclerView.Adapter<RecipientListAdapter.ViewHolder> {


    private Context context1;
    List<SentMessage> sentMessages = new ArrayList<>();
    SimpleDateFormat datetimeformat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

    public RecipientListAdapter(Context context2, List<SentMessage> sentMessages) {
        this.sentMessages = sentMessages;
        context1 = context2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView firstname;
        private TextView lastname;
        private TextView datetime;
        private TextView otp;
        public ViewHolder(View v) {
            super(v);
            firstname = (TextView)v.findViewById(R.id.frstnme);
            lastname = (TextView)v.findViewById(R.id.lstnme);
            datetime = (TextView)v.findViewById(R.id.datetime);
            otp = (TextView)v.findViewById(R.id.otp);
        }
    }

    @Override
    public RecipientListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(context1).inflate(R.layout.recipientlistadapter, parent, false);

        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Vholder, final int position) {
        SentMessage sentMessage = sentMessages.get(position);
        Vholder.firstname.setText(sentMessage.getFirstName());
        Vholder.lastname.setText(sentMessage.getLastName());
        Date d = new Date(sentMessage.getTime());
        Vholder.datetime.setText(datetimeformat.format(d));
        Vholder.otp.setText("OTP: "+sentMessages.get(position).getOtp());

    }



    @Override
    public int getItemCount() {

        return sentMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
